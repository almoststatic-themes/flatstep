<?php
// ----------------------------------------------------------------------------
// sample email send with PHPMailer
// see: https://github.com/PHPMailer/PHPMailer
//
// write static pages, put them under apache or nginx with php and you can
// call this with ajax call.
// If you don't use ajax, you must provide a convenient way to return to your
// static pages.
//

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\SMTP;

require "PHPMailer/PHPMailer.php";
require "PHPMailer/Exception.php";
require "PHPMailer/SMTP.php";

// configure with your data
$fromEmail = 'info@yourdomain.com';
$fromName = 'YourName';
$sendToEmail = 'info@yourdomain.com';
$sendToName = 'YourName contact form';
$subject = 'Message from yourdomain.com';
$okMessage = 'Thanks, we will reply asap.';
$errorMessage = "Oops we got an error, try later.";
$emailTextHtml = "";

// configure with your data
$mail = new PHPMailer(true);
$mail->isSMTP();
$mail->Host       = '';
$mail->SMTPAuth   = true;
$mail->Username   = '';
$mail->Password   = '';
$mail->SMTPSecure = 'ssl';
$mail->Port       = 465;

error_reporting(E_ALL & ~E_NOTICE);
try
{
    if(count($_POST) == 0) throw new \Exception('Form is empty');

    $emailTextHtml .= "<table>\n";

    foreach ($_POST as $key => $value) {
        $emailTextHtml .= "<tr><th>$key</th><td>$value</td></tr>\n";
    }
    $emailTextHtml .= "</table>\n";

    $mail->setFrom($fromEmail, $fromName);
    $mail->addAddress($sendToEmail, $sendToName);

    $mail->isHTML(true);

    $mail->Subject = $subject;
    $mail->msgHTML($emailTextHtml);

    if(!$mail->send()) {
        throw new \Exception('I could not send the email.' . $mail->ErrorInfo);
    }
    $responseArray = array('type' => 'success', 'message' => $okMessage);
}
catch (\Exception $e)
{
    $responseArray = array('type' => 'danger', 'message' => $e->getMessage());
}

// if requested by AJAX request return JSON response
if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
    $encoded = json_encode($responseArray);
    header('Content-Type: application/json');
    echo $encoded;
}
// else just display the message
else {
    echo $responseArray['message'];
}

// write a log
$file = 'send_mail.log';
$value  = "==============================================\n";
$value .= date("Y-m-d G:i") . "\n";
$value .= $responseArray['message'] . "\n";
$value .= "==============================================\n";
$value .= $emailTextHtml . "\n\n";
file_put_contents($file, $value, FILE_APPEND | LOCK_EX);
