---
title: Forms
author: Claudio Driussi
widgets_envelope: ['<div class="container">', "</div>"]

script: |
  $("#form1").submit(send_mail);

content:
  - type: text_html
    id: index
    text: |
      # Forms

      Form is one of the most sophisticated widget of flatstep. This is needed
      because form needs can vary a lot.

      Form widget is composed by "inputs" each type of input has it's
      corresponding html input type. Between inputs, a special input called
      "html" can be added to add some text.

      Each input has the "column" property which let Bootstrap to reflow
      widgets in responsive mode.

      Forms can be splitted into several forms to let you embed complex form
      within other widgets such as accordions or tabs. And off course you can
      embed forms within other widgets.

      To submit forms you need some dynamic content or JavaScript.

  - type: divider
    class: divider-left
    title: Form 1
    text: |
      All controls and some html text.

  - type: form
    id: form1
    class: g-3
    action: "media/php/send_mail.php"
    target: _self
    method: POST
    input:
      - type: html
        id: divider
        column: 12
        text: |
          ## Some common input types

      - type: text
        id: firstname
        column: 6
        label: First Name
        value: John

      - type: text
        id: lastname
        column: 6
        label: Last Name
        value: Doe

      - type: text
        id: address
        column: 12
        label: Address
        placeholder: ZipCode City State

      - type: email
        id: email
        column: 6
        group_before: email
        group_after: "@"
        label: Your email
        pattern: '[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$'
        attr: required

      - type: password
        id: password
        column: 6
        label: Password
        placeholder: min 8 max 20 chars
        attr: required

      - type: textarea
        id: comment
        column: 12
        group_before: ...
        rows: 3
        label: Write here your comment
        placeholder: Your comment
        attr: required

      - type: html
        id: divider
        column: 12
        text: |
          ## Check boxes, Radio buttons and so on

      - type: html
        id: chk-inline
        column: 2
        text: "**Inline controls:**"

      - type: checkbox
        column: 6
        id: chk-cookies
        inline: true
        ckh_items:
          [
            [
              "cookies",
              'I accept <a href="{{get_url(''cookies'')}}">cookies</a>',
              "",
            ],
            ["advertising", "I accept advertising", "checked"],
            ["chk-mail", "Send me email", "checked"],
          ]

      - type: radio
        column: 4
        id: radio
        inline: true
        ckh_items:
          [
            ["r1", "Radio1", "checked"],
            ["r2", "Radio2", ""],
            ["r3", "Radio3", "disabled"],
          ]

      - type: html
        id: chk-inline
        column: 2
        text: "**Stacked controls:**"

      - type: checkbox
        column: 6
        id: schk-cookies
        ckh_items:
          [
            [
              "cookies",
              'I accept <a href="{{get_url(''cookies'')}}">cookies</a>',
              "",
              "form-switch",
            ],
            ["advertising", "I accept advertising", "checked", "form-switch"],
            ["chk-mail", "Send me email", "checked", "form-switch"],
          ]

      - type: radio
        column: 4
        id: sradio
        ckh_items:
          [
            ["r1", "Radio1", "checked"],
            ["r2", "Radio2", ""],
            ["r3", "Radio3", "disabled"],
          ]

      - type: select
        id: states
        column: 6
        label: Select state
        selected: Italy
        options: [Italy, Austria, Germany, France, "United Kingdom", Spain, US]
        attr: required

      - type: range
        id: like
        column: 6
        min: 1
        max: 5
        step: 0.5
        value: 3
        label: How much do you like this site?

      - type: hidden
        id: name
        value: form1

      - type: submit
        id: submit
        class: btn btn-primary
        value: Sign in

  - type: divider
    class: divider-left
    title: Form 2
    text: |
      Form splitted in several widgets within an accordion. You must set the
      "opened" property to true on each form and then close it on the last widget.

  - type: form
    id: form2
    envelope: ["", ""]
    action: "#"
    method: get
    opened: true

  - type: accordion
    id: fisa1
    open: True
    cards:
      - button: "Some field"
        text:
          content:
            - type: form
              id: form2
              class: g-3
              opened: true
              input:
                - type: text
                  id: firstname
                  column: 6
                  label: First Name
                  placeholder: Your name
                  attr: required

                - type: text
                  id: lastname
                  column: 6
                  label: Last Name
                  placeholder: Your last name
                  attr: required

      - button: "Other fields"
        text:
          content:
            - type: form
              id: form2
              class: g-3
              opened: true
              input:
                - type: text
                  id: request
                  column: 12
                  label: Reason of your request
                  attr: required

                - type: textarea
                  id: comment
                  column: 12
                  rows: 3
                  label: Write here your comment
                  placeholder: Your comment

      - button: "Submit"
        text:
          content:
            - type: form
              id: form2
              class: g-3
              opened: true
              input:
                - type: checkbox
                  column: 6
                  id: schk-cookies
                  ckh_items: [["cookies", "I accept cookies", "required", ""]]

                - type: html
                  id: chk-inline
                  column: 6
                  text: |
                    <a href="{{get_url('cookies')}}">see cookies policy</a>

                - type: submit
                  id: submit
                  class: btn btn-primary
                  value: Sign in

  - type: form
    id: form2
    envelope: ["", ""]
    opened: false

  - type: divider
    class: divider-left
    title: Form 3
    text: |
      Form with some style

  - type: style
    style: |
      #form3 input[type="text"], #form3 select, #form3 textarea {
        background-color: aliceblue;
      }
      #form3 input:hover, #form3 select:hover, #form3 textarea:hover {
        background-color: azure;
      }

  - type: form
    id: form3
    class: g-3 grid-border-20
    style: |
      #widget_id {background-color: ivory;}
    action: "#"
    target: _self
    method: get
    input:
      - type: text
        id: firstname
        column: 6
        label: First Name
        placeholder: Your name
        attr: required

      - type: text
        id: lastname
        column: 6
        label: Last Name
        placeholder: Your last name
        attr: required

      - type: checkbox
        column: 6
        id: chk-cookies
        inline: true
        ckh_items:
          [
            ["chk1", "check1", ""],
            ["chk2", "check2", ""],
            ["chk3", "check3", ""],
          ]

      - type: select
        id: states
        column: 6
        label: Select state
        selected: Italy
        options: [Italy, Austria, Germany, France, "United Kingdom", Spain, US]
        attr: required

      - type: textarea
        id: comment
        column: 12
        rows: 3
        label: Write here your comment
        placeholder: Your comment
        attr: required

      - type: submit
        id: submit
        class: btn btn-success
        value: Sign in

  - type: divider
    class: divider-left
    title: Form 4
    text: |
      Form within a grid

  - type: divider

  - type: grid
    disabled: false
    id: widget
    class: border-round-10 thin-border
    columns:
      - column: 1
        class: text-center
        text: '<i class="bi bi-envelope-open" style="font-size: 3rem"></i>'

      - column: 11
        text:
          content:
            - type: form
              id: form4
              class: g-3
              action: "#"
              method: post
              ajax: send_mail
              input:
                - type: html
                  text: |
                    # Sens us an email

                - type: text
                  id: name
                  column: 6
                  label: Your name
                  attr: required

                - type: email
                  id: email
                  column: 6
                  group_after: "@"
                  label: Your email
                  pattern: '[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$'
                  attr: required

                - type: textarea
                  id: comment
                  column: 12
                  label: Notes

                - type: submit
                  id: submit
                  column: 2
                  class: btn btn-secondary
                  value: Send
