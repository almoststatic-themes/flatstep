---
title: Home page
author: Claudio Driussi
widgets_envelope: ['<div class="container">', "</div>"]

hero:
  id: hero
  image: images/hero/hero4.jpg
  title: Welcome on FlatStep
  height: 320
  text: |
    An Almoststatic theme built with Bootstrap

content:
  - type: style
    disabled: true
    style: "div { border-style: dashed; border-width: 1px; border-color: darkgray; }"

  - type: divider
    disabled: false
    height: 10px

  - type: text_html
    id: index
    text: |
      # Home page

      Welcome on **FlatStep**, an [Almoststatic](https://pypi.org/project/almoststatic/)
      theme used to build static and dynamic sites with [Bootstrap](https://getbootstrap.com/) css package.

      **FlatStep** comes after [FullStep](https://gitlab.com/almoststatic-themes/fullstep)
       theme and it aims to solve some issues emerged during development.

  - type: divider
    disabled: false
    height: 10px

  - type: grid
    disabled: false
    envelope:
      [
        '<div class="bg-light"><div class="container pt-2 pb-2">',
        "</div></div>",
      ]
    id: links
    class: border-round-10 thin-border
    columns:
      - column: 1
        class: text-center
        text: '<i class="bi bi-link" style="font-size: 3rem"></i>'

      - column: 11
        text: |
          #### Some links

          [Almoststatic](https://pypi.org/project/almoststatic/) on PyPI package index <br>
          [Almoststatic](https://gitlab.com/claudio.driussi/almoststatic) repository <br>
          [Almoststatic](https://almoststatic.readthedocs.io/en/latest/) documentation <br>
          [Bootstrap](https://almoststatic.readthedocs.io/en/latest/) site<br>
          [FlatStep](https://gitlab.com/almoststatic-themes/flatstep) theme repository<br>
          [FullStep](https://gitlab.com/almoststatic-themes/fullstep) theme repository (deprecated)

  - type: divider
    disabled: false
    height: 10px

  - type: text_html
    id: goals
    text: |
      ## The goals of FlatStep are

      - Let easy to build clean and nice static sites
      - Better usage of `container` and `container-fluid` Bootstrap classes
      - Thin and versatile integration with Bootstrap components
      - Let easier to derivate themes
      - Let easier to customize widgets with classes and inline style directives.

  - type: divider
    disabled: false
    height: 10px

  - type: text_html
    id: guidelines
    disabled: false
    envelope:
      [
        '<div class="bg-light"><div class="container pt-2 pb-2">',
        "</div></div>",
      ]
    text: |
      ## Guidelines

      To achieve our goals, we wrote only a minimum amount of widgets very
      closed to the Bootstrap components, plus some general propose widgets.

      Each widget let you to add classes to all elements of the component, plus
      has a unique id for each element. This let you to add style where you
      need. Look at [grid examples]({{get_url(^^widgets/grid^^)}}) to see how
      to simulate the old *Jumbotron* Bootstrap component.

      General propose widgets are widgets widely used to write content or to do
      some special tasks. See [text]({{get_url(^^widgets/text^^)}}) and
      [semantic]({{get_url(^^widgets/semantic^^)}}), and also `divider`, `style`,
      `script` and so on.

      To handle container Bootstrap classes, in the base page all widgets are
      within a `container-flex` `div` and in each page widgets are included in
      a fixed `container` envelope this let to use flex container when needed, see
      many examples in source code.

  - type: divider
    disabled: false
    height: 10px

  - type: grid
    disabled: false
    id: widget
    class: border-round-10 thin-border
    columns:
      - column: 1
        class: text-center
        text: '<i class="bi bi-question-circle" style="font-size: 3rem"></i>'

      - column: 11
        text: |
          #### But what is a widget?

          See the [Almoststatic documentation](https://almoststatic.readthedocs.io/en/latest/).

          But in short, widget is a directive in [https://yaml.org/](https://yaml.org/) format
          which generate a piece of html inserted into your page.

  - type: divider
    disabled: false
    height: 10px

  - type: grid
    disabled: false
    id: next
    class: border-round-10 thin-border
    columns:
      - column: 1
        class: text-center
        text: '<i class="bi bi-question-circle" style="font-size: 3rem"></i>'

      - column: 11
        text: |
          #### And what next?

          See at our [installing tutorial]({{get_url(^^tutorial^^)}}) to build a local live version of this site
          then take a look at widgets examples and dig into source code.

          Then you can see some widgets capabilities on our demos.

          At last you can read some [blog pages]({{get_url(^^blog^^)}})

  - type: divider
    disabled: false
    height: 10px
