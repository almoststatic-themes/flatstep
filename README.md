# FlatStep

**FlatStep** is a theme for [Almoststatic](https://pypi.org/project/almoststatic/), see the [documentation](https://almoststatic.readthedocs.io)

FlatStep uses [Bootstrap](https://getbootstrap.com/) as css engine, and let you to compile your own customized version with sass.

FlatStep uses [Bootswatch](https://bootswatch.com/) as base css configuration and writes css theme starting from it.

## Demo sites

You can see a [working demo](https://flatstep.claudiodriussi.it/) of this site without compiling it.

There are also some derived themes that you can see:

- [Greyscale theme](https://greyscale.claudiodriussi.it/) get the [source code](https://github.com/claudiodriussi/almoststatic-theme-greyscale)
- [Resume theme](https://resume.claudiodriussi.it/) get the [source code](https://github.com/claudiodriussi/almoststatic-theme-resume)

## Quick start

This tutorial lets you to install a working demo of FlatStep theme for [Almoststatic](https://pypi.org/project/almoststatic/).
It is tested on Linux, adjust it if you are on Windows or Mac.

Before you start, you need some programming tools on your pc:

- [Python](https://www.python.org/)
- [sass](https://sass-lang.com/)

You don't need to compile bootstrap, but if you like to do so, you need:

- [nodejs](https://nodejs.org)
- [npm](https://www.npmjs.com/)

A good programmer editor or ide is not required but advised, there are many on web, my choose is [Visual studio code](https://code.visualstudio.com/)

Install everything and go on.

I advise to make a folder for all your Almoststatic themes and within it make an Python environment similar to one described [here](https://almoststatic.readthedocs.io/en/latest/quickstart.html).

Copy into it the `flatstep` folder.

Then download [Bootstrap source files](https://getbootstrap.com/docs) and extract the files on folder `bootstrap`

Now your folder should look like this

    ├── bootstrap
    │   └── <...>
    ├── flatstep
    │   ├── content
    │   │   ├── media
    │   │   ├── pages
    │   │   ├── scss
    │   │   ├── templates
    │   │   └── config.yaml
    │   ├── custom
    │   │   └── <...>
    │   ├── flaskapp.py
    │   └── write_static.py
    └── myvenv
        └── <...>

And don't forget to copy js bundle files from bootstrap to your media folder with:

    $ cd flatstep
    $ cp ../bootstrap/dist/js/bootstrap.bundle.min.* content/media/js/

Now you are ready to start. Before all, goto on your main folder and run:

    $ python3 -m venv myvenv
    $ source myvenv/bin/activate
    $ pip install -U pip
    $ pip install almoststatic
    $ pip install pyftpsync

Now you need to compile the css with sass, from your main folder:

    $ cd flatstep/content/scss
    $ ./compile_bootstrap.sh &
    $ ./compile_main.sh &

If everything is ok, now you will see some `*.css` files on your `content/media/css` folder, else you have to fix your environment. And each time you change something in you `scss` files they will be recompiled

Now you are ready to develop your site. From main folder:

    $ cd flatstep
    $ python flaskapp.py

And navigate to `http://127.0.0.1:5000/` you should see the site and you can customize it with your contents.

When you are done, you can customize the `write_static.py` to write your static site.
## Develop derived sites

FlatStep comes with it's own theme. When you develop your site you need to write your contents and add your madia static files.

But often you need also to change some css from standard and to write some templates to adjust headers, footers and so on.

This is easy with FlatStep, the standard distribution contain an example. Simply go to custom folder, recompile css and run flaskapp.py From main folder:

    $ source myvenv/bin/activate
    $ cd flatstep/custom/content/scss/
    $ ./compile_bootstrap.sh &
    $ ./compile_main.sh &
    $ cd ../../
    $ python flaskapp.py

And navigating to `http://127.0.0.1:5000/` you should see a derived site.

But to keep things tidy, could be a good idea to put sites outside FlatStep, for example:

    ├── bootstrap
    │   └── <...>
    ├── flatstep
    │   └── <...>
    ├── mysites
    │   ├── site1
    │   │   └── <...>
    │   └── site2
    │       └── <...>
    └── myvenv
        └── <...>

To start a new site copy `custom` folder from `flatstep` to a location of your choice, i.e. `mysites/site1`.

Now you have to adjust paths into your `content/config.yaml` file, write:

    config:
    templates: ["custom_templates", "../../flatstep/templates"]
    static_url: "/"
    cache: False

This teach to Flask and Jinja2 to search templates first in `custom_templates`
and if the files is not found in '../../flatstep/templates'

Then you have to adjust paths into scss source files. In `main.scss` change the row:

    @import "../../../content/scss/theme/widgets";

to

    @import "../../../../flatstep/content/scss/theme/widgets";

In `custom_bootstrap.scss` the row:

    @import "../../../../bootstrap/scss/bootstrap";

should be ok, but if needed change it to the one which is ok.

Off course, you have to tune relative paths until they are all correct.

When all is ok, you can recompile css and launch `flaskapp.py` as usual and start to add your contents and your style.

Enjoy!.


